function [belief, value]= fictitiousPlay(A, K)

    N=length(size(A))-1; %% compute random initialization for the mixed path
    belief=cell(N,1);
    for i=1:N
        if 1
            belief{i}=rand(size(A,i),1); %random belief
        else
            belief{i}=ones(size(A,i),1); %uniform belief
        end
        belief{i}=belief{i}/sum(belief{i}); %normalize to get distribution
    end
    
    %% iterate fictitious play
    for k=1:K
        
        %% compute probability - weighted outcomes
        Ay=A;
        for i=1:N
            reps=size(A);
            reps(i)=1;
            shape=ones(1,N+1);
            shape(i)= length(belief{i});
            yi=reshape(belief{i},shape);
            yi=repmat(yi,reps);
            %for player i ’ s outcomes do not multiply by its own
            %belief to eventually compute the best response
            str=['yi(',repmat(':,',1,N),num2str(i),')=1;'];
            eval(str);
            Ay=Ay.*yi;
        end % for i = 1:N
        
        %% compute best response
        y=cell(N,1);
        for i=1:N
            % average outcomes over actions of other players
            str=['Ay(',repmat(':,',1,N), num2str(i),')'];
            S=eval(str);
            for j=1:N
                if j~=i
                    S=sum(S,j);
                end
            end
            % compute best responses
            [~,j]=min(S,[],i); %pure best response
            y{i}= zeros(size(S,i),1);
            y{i}(j)=1; % mixed best response
        end %for i = 1:N
        
        %% update belief
        for i=1:N
            belief{i}=(k*belief{i}+y{i})/(k+1);
        end %fori=1: N
    end %for k = 1:K 
    
    %% compute final value
    value=nan(N,1); %compute probability - weighted outcomes
    Ay=A;
    for i = 1:N
        reps=size(A);
        reps(i)=1;
        shape=ones(1,N+1);
        shape(i)= length(belief{i});
        yi=reshape(belief{i},shape);
        yi=repmat(yi ,reps);
        Ay=Ay.*yi;
    end %for i = 1:N
    for i = 1:N %average outcomes over actions of all players
        str=['Ay(',repmat(':,',1,N),num2str(i),')'];
        S=eval(str);
        for j = 1:N
            S=sum(S,j);
        end
        value(i)=S;
    end
end