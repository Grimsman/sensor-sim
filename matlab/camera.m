% camera - get camera measurement - calculates (23) in the paper
%
% q: state of target. it is assumed that the first two elements are the
%    (x, y) position
% p: state of the vehicle. it is assumed that the first two elements are
%    the (x, y) position, and the third is the angle of the heading
% fx: the focal length of the camera
% ox: the focal center of the image plane
%
function cam = camera(q, p, fx, ox)
   A = [fx, ox; 0, 1];
   theta = p(3);
   Rc = [cos(theta), -sin(theta); sin(theta), cos(theta)];
   M = [1 0] * A * Rc;
   m = [0 1] * A * Rc;
   q_pos = q(1:2);
   p_pos = p(1:2);
   cam = (M * (q_pos - p_pos)) / (m * (q_pos - p_pos));
end