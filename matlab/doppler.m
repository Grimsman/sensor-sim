% doppler - get doppler measurement - calculates (22) in the paper
%
% q: state of target. it is assumed that the first two elements are the
%    (x, y) position
% p: state of the vehicle. it is assumed that the first two elements are
%    the (x, y) position, and the third is the angle of the heading
% dq: time derivative of q. it is assumed that the first two elements are
%    the time derivativdqe of the (x, y) position
% fc: carrier frequency of the transmitter
% fr: carrier frequency of the receiver
% v: constant speed of the dubins car
%
function dop = doppler(q, p, dq, fc, fr, v)
    c = 299792458;
    lambda = c / fc;
    theta_lambda = lambda * abs(fc - fr);
    theta = p(3);
    p_pos = p(1:2);
    q_pos = q(1:2);
    dp = [v * cos(theta); v * sin(theta)];
    drho = ((q_pos - p_pos)' * (dq(1:2) - dp(1:2))) / norm(q_pos-p_pos, 2);
    dop = theta_lambda - drho;
end