% time_of_arrival - get time of arrival measurement - calculates (20) in
% the paper
%
% q: state of target at time step k. it is assumed that the first two
%    elements are the (x, y) position
% p: state of the vehicle at time step k. it is assumed that the first two
%    elements are the (x, y) position
% t: time step at which we are evaluating time of arrival measurement
% T: parameter to be estimated
% T0: parameter to be estimated
%
function toa = time_of_arrival(q, p, k, T, T0)
    c = 299792458;
    p_pos = p(1:2);
    q_pos = q(1:2);
    rho = norm(q_pos - p_pos, 2);
    toa = rho + c*T*k + c*T0;
end