import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
import itertools as it

max_coord = 100
min_coord = -100
max_dist = 200
num_pts = 10

########################################################################################
# ScenarioPath object that stores information about an agent's path
########################################################################################


class ScenarioPath:

    ########################################################################################
    # __init__ initialize an object
    #    inputs
    #        start (tuple): a 2-tuple of int representing the x,y coordinates of the starting position of the path
    #        stop (tuple): a 2-tuple of int representing the x,y coordinates of the ending position of the path
    #        dist (float): each path is an arc along a circle, and the center of that circle lies on the bisecting line between
    #                            start and stop. dist is how far away from the midpoint between start and stop along that line that
    #                            the center of that circle is. +/- mean different directions along that line.
    ########################################################################################

    def __init__(self, start, stop, dist):
        # find the midpoint between the two
        midpoint = (start + stop) / 2
        if start[1] == stop[1]:
            slope = np.inf
        else:
            slope = -(start[0] - stop[0]) / (start[1] - stop[1])
        theta_c = np.arctan(slope)
        self.center = (-dist * np.cos(theta_c) +
                       midpoint[0], -dist * np.sin(theta_c) + midpoint[1])
        radius = np.linalg.norm(self.center - start, 2)
        theta_start = np.arctan2(
            start[1] - self.center[1], start[0] - self.center[0])
        theta_stop = np.arctan2(
            stop[1] - self.center[1], stop[0] - self.center[0])
        if np.abs(theta_start - theta_stop) <= np.pi:
            self.theta = np.linspace(theta_start, theta_stop, num_pts)
        elif theta_start <= 0:
            self.theta = np.linspace(
                theta_start + 2 * np.pi, theta_stop, num_pts)
        else:
            self.theta = np.linspace(
                theta_start - 2 * np.pi, theta_stop, num_pts)
        if self.theta[0] - self.theta[int(num_pts / 2)] >= 0:
            self.clockwise = True
        else:
            self.clockwise = False
        self.path_pts = [(self.center[0] + radius * np.cos(t),
                          self.center[1] + radius * np.sin(t)) for t in self.theta]
        self.start = start
        self.stop = stop

    ########################################################################################
    # plot the path
    #    inputs
    #        color (str): color of the line
    #        include_start (bool): if True, plots a circle at the start point
    #        include_stop (bool): if True, plots a circle at the stop point
    #        ax (pyplot.Axes): Axes object on which to plot path. if None, then plt.plot is used
    ########################################################################################

    def plot(self, color='k', include_start=True, include_stop=False, ax=None):

        if not ax:
            plt.plot([pt[0] for pt in self.path_pts], [pt[1]
                                                       for pt in self.path_pts], c=color)
            if include_start:
                plt.scatter(self.start[0], self.start[1], c='g')
            if include_stop:
                plt.scatter(self.stop[0], self.stop[1], c='r')
        else:
            ax.plot([pt[0] for pt in self.path_pts], [pt[1]
                                                      for pt in self.path_pts], c=color)
            if include_start:
                ax.scatter(self.start[0], self.start[1], c='g')
            if include_stop:
                ax.scatter(self.stop[0], self.stop[1], c='r')

    ########################################################################################
    # __getitem__ retrieve the position at time i
    #    inputs
    #        i (int): time at which to return position
    #    outputs
    #        2-tuple of x,y position at time i
    ########################################################################################

    def __getitem__(self, i):
        if i >= len(self.path_pts):
            return
        return self.path_pts[int(i)]


########################################################################################
# an Agent for the Scenario
########################################################################################

class ScenarioAgent:

    ########################################################################################
    # __init__ initialize an agent
    #    inputs
    #        path (ScenarioPath): the path that the agent takes in the scenario
    #        agent_id (int): the agent's id
    #        target_pos (list): list of 2-tuples, see Scenario.__init__() for example
    #        camera_view_angle (float): the field of view angle, see Scenario.__init__() for example
    #        color (str): str representing the color that the path and positions are on the plots
    #    outputs: None
    ########################################################################################

    def __init__(self, path, agent_id, target_pos, camera_view_angle, color):
        self.path = path
        self.target_pos = target_pos
        self.fims = np.empty(
            (num_pts, 2 * len(target_pos), 2 * len(target_pos)))
        self.agent_id = agent_id
        self.camera_view_angle = camera_view_angle
        self.color = color

    ########################################################################################
    # pos get the position of the agent at time t
    #    inputs
    #        t (int): the time at which we are querying the agent's position
    #    outputs
    #        (tuple): a 2-tuple of floats which are the xy position of the agent at time t
    ########################################################################################

    def pos(self, t):
        return self.path[t]

    ########################################################################################
    # add_fim add a fim the agent at time t
    #     inputs
    #        fim (np.array): 2x2 array representing fim
    #        t (int): time at which to add
    #    outputs: None
    ########################################################################################

    def add_fim(self, fim, t):
        self.fims[t] = fim

    ########################################################################################
    # choose_greedy greedily choose k fims
    #    inputs
    #        fim_sent (np.array): 2x2 array representing the overall fim that has been sent to the satellite thus far
    #        k (int): the number of fims that are sent to the satellite
    #        f (function): the submodular set function
    #        ax (matplotlib.pyplot.Axes): Axes on which to plot the visualization of the algorithm. None for no visualization
    #    outputs
    #        fim_sent (np.array): updated fim_sent np.array
    #        fims_passed: not used
    #        chosen_inds (list): a list of ints representing which fims were chosen by the algotrithm
    #        passed_inds: not used
    ########################################################################################

    def choose_greedy(self, fim_sent, k, f, ax=None):
        fim_sent, chosen_inds = self._greedy(
            fim_sent, self.fims, k, f, 'greedy', ax=ax)
        return fim_sent, [], chosen_inds, []

    ########################################################################################
    # choose_auggreedy_opt use the augmented greedy algorithm while solving the full argmax of decision and
    #                                         passing rule. inputs/outputs are the same as choose_auggreedy
    ########################################################################################

    def choose_greedy_opt(self, fim_sent, k, f, ax=None):
        fim_sent, chosen_inds = self._greedy(
            fim_sent, self.fims, k, f, 'greedy_opt', fims_per_iter=k, ax=ax)
        return fim_sent, [], chosen_inds, []

    ########################################################################################
    # choose_random randomly choose k fims
    #    inputs
    #        k (int): the number of fims that are sent to the satellite
    #    outputs
    #        fim_sent (np.array): updated fim_sent np.array
    #        fims_passed: not used
    #        chosen_inds (list): a list of ints representing which fims were chosen
    #        passed_inds: not used
    ########################################################################################

    def choose_random(self, k):
        inds = np.random.randint(0, len(self.fims), k)
        return self.fims[k], [], inds, []

    ########################################################################################
    # _greedy greedily choose k fims
    #    inputs
    #        fim_sent (np.array): 2x2 np.array representing the fim which has already been sent to the satellite
    #        my_fims (np.array): nx2x2 np.array where my_fims[i] is a fim from which to choose
    #        num (int): the number of fims to choose from my_fims
    #        f (function): global submodualr set function
    #        alg_name (str): name of the algorithm to use
    #        ax (matplotlib.pyplot.Axes): axes on which to plot the visualization
    #        update_fim_sent (bool): if True, fim_sent matrix will be updated with the fims chosen here
    #        exclude_inds (list): list of ints. if i in exclude_inds, then my_fims[i] will be excluded in the search
    #        oafun (function): function that can plot things about another agent
    #        marker (str): this is the marker for the chosen measurements on the plot
    ########################################################################################

    def _greedy(self, fim_sent, my_fims, num, f, alg_name, fims_per_iter=1, ax=None, update_fim_sent=True, exclude_inds=[], oafun=None, marker='X'):
        if not update_fim_sent:
            fim_sent = np.copy(fim_sent)
        if ax is not None:
            fig = plt.gcf()

        chosen = []

        ########################################################################################
        # _get_score gets the score for my_fims[i]
        #    inputs
        #        i (int): index in my_fims
        #    outputs
        #        s (float): the score given to the fim
        ########################################################################################

        def _get_score(c):
            s = -np.inf
            if fims_per_iter > 1:
                chosen_fims = [my_fims[i] if i not in set(chosen) | set(
                    exclude_inds) else np.zeros((fim_sent.shape)) for i in c]
            elif c[0] not in set(chosen) | set(exclude_inds):
                chosen_fims = [my_fims[c[0]]]
            else:
                chosen_fims = []

            if len(chosen_fims) > 0:
                s = f([fim_sent] + chosen_fims)
            return s

        comb2reg = {i: c for i, c in enumerate(
            it.combinations(range(len(my_fims)), fims_per_iter))}

        while len(chosen) < num:
            scores = np.array(
                [_get_score(c) for c in it.combinations(range(len(my_fims)), fims_per_iter)])
            t_max = np.argmax(scores)

            if ax is not None:
                ts = np.where(scores >= 0)[0]
                scores[scores < 0] = 0
                x_pos = [j for j in range(len(my_fims))]
                for t in ts:
                    artists = []
                    if t < num_pts:
                        artists = self.plot_view(ax, t)
                    else:
                        artists = oafun(t, ax, alg_name)

                    color = ['blue' for j in range(len(my_fims))]
                    color[t] = 'green'
                    bars = fig.axes[1].bar(x_pos, scores, color=color)
                    artists.append(bars)
                    fig.axes[1].set_xticks(x_pos)
                    if scores[t_max] == 0:
                        ytop = 1
                    else:
                        ytop = scores[t_max] * 1.1
                    fig.axes[1].set_ylim(bottom=0, top=ytop)

                    fig.suptitle('Agent: {}, Algorithm: {}'.format(
                        self.agent_id, alg_name))

                    plt.waitforbuttonpress()
                    for artist in artists:
                        if type(artist) == list:
                            for m in artist:
                                m.remove()
                        else:
                            artist.remove()

                if t_max < num_pts:
                    pos_max = self.pos(t_max)
                    ax.scatter(pos_max[0], pos_max[1],
                               marker=marker, color=self.color)
                else:
                    oafun(t_max, ax, alg_name, plot_type='mark',
                          marker_color=self.color)

            inds_lst = list(comb2reg[t_max])
            chosen += inds_lst
            fim_sent += my_fims[inds_lst].sum(axis=0)
        return fim_sent, chosen

    ########################################################################################
    # choose_auggreedy use the augmented greedy algorithm to choose k
    #    inputs
    #        fim_sent (np.array): 2x2 array representing the overall fim that has been sent to the satellite thus far
    #        fims_passed (list): list of 2x2 np.arrays represeting fims that have been passed along by other agents
    #        pc_inds (list): list of ints which represent the indices of fims which have been passed by some agent and then
    #                               chosen by another agent lowest value can be num_pts. example: [10, 13, 11]
    #        k (int): the number of fims that are sent to the satellite
    #        m (int): the number of fims that are passed forward to future agents
    #        f (function): the submodular set function
    #        ax (matplotlib.pyplot.Axes): Axes on which to plot the visualization of the algorithm. None for no visualization
    #    outputs
    #        fim_sent (np.array): updated fim_sent np.array
    #        fims_passed (list): update list of fims_passed
    #        chosen_inds (list): a list of ints representing which fims were chosen by the algotrithm
    #        passed_inds (list): a list of ints representing which fims were passed by this agent to future agents
    ########################################################################################

    def choose_auggreedy(self, fim_sent, fims_passed, pc_inds, k, m, f, ax=None, oafun=None):
        fim_sent, chosen_inds = self._greedy(fim_sent, np.append(
            self.fims, fims_passed, axis=0), k, f, 'augmented_greedy', ax=ax, update_fim_sent=True, exclude_inds=pc_inds, oafun=oafun)
        if m > 0:
            _, passed_inds = self._greedy(
                fim_sent, self.fims, m, f, 'augmented_greedy', ax=ax, update_fim_sent=False, exclude_inds=pc_inds | set(chosen_inds), marker='x')
        else:
            passed_inds = []
        fims_passed = np.append(fims_passed, self.fims[passed_inds], axis=0)
        return fim_sent, fims_passed, chosen_inds, passed_inds

    ########################################################################################
    # choose_auggreedy_opt use the augmented greedy algorithm while solving the full argmax of decision and
    #                                         passing rule. inputs/outputs are the same as choose_auggreedy
    ########################################################################################

    def choose_auggreedy_opt(self, fim_sent, fims_passed, pc_inds, k, m, f, ax=None, oafun=None):
        fim_sent, chosen_inds = self._greedy(fim_sent, np.append(self.fims, fims_passed, axis=0), k, f,
                                             'augmented_greedy_opt', fims_per_iter=k, ax=ax, update_fim_sent=True, exclude_inds=pc_inds, oafun=oafun)
        if m > 0:
            _, passed_inds = self._greedy(fim_sent, self.fims, m, f, 'augmented_greedy_opt', fims_per_iter=m,
                                          ax=ax, update_fim_sent=False, exclude_inds=pc_inds | set(chosen_inds), marker='x')
        else:
            passed_inds = []
        fims_passed = np.append(fims_passed, self.fims[passed_inds], axis=0)
        return fim_sent, fims_passed, chosen_inds, passed_inds

    ########################################################################################
    # choose k fims to send to the satellite and (possibly) m fims to send to future agents. inputs are same as
    #              choose_auggreedy
    #    outputs
    #        fim_sent (np.array): updated fim_sent np.array
    ########################################################################################

    def choose(self, fim_sent, fims_passed, pc_inds, f, k, m, method='greedy', ax=None, oafun=None):
        if method == 'greedy':
            return self.choose_greedy(fim_sent, k, f, ax=ax)
        if method == 'greedy_opt':
            return self.choose_greedy_opt(fim_sent, k, f, ax=ax)
        if method == 'random':
            return self.choose_random(k)
        if method == 'augmented_greedy':
            return self.choose_auggreedy(fim_sent, fims_passed, pc_inds, k, m, f, ax=ax, oafun=oafun)
        if method == 'augmented_greedy_opt':
            return self.choose_auggreedy_opt(fim_sent, fims_passed, pc_inds, k, m, f, ax=ax, oafun=oafun)

        return fim_sent

    ########################################################################################
    # can_see determine whether the agent can see the target at time t
    #    inputs
    #        t (int): time at which we are testing whether the agent can see the target
    #        tpos (tuple): 2-tuple of x,y location of the target
    #    outputs
    #        seeable (bool): True if the agent can see the target at time t, False otherwise
    ########################################################################################

    def can_see(self, t, tpos):
        pos = self.pos(t)
        theta = self.path.theta[t]
        if not self.path.clockwise:
            theta += np.pi
        theta2 = np.arctan2(tpos[1] - pos[1], tpos[0] - pos[0])
        view_angle = theta - theta2
        if view_angle < -np.pi:
            view_angle += 2 * np.pi
        elif view_angle > np.pi:
            view_angle -= 2 * np.pi
        seeable = True
        if abs(view_angle) > self.camera_view_angle / 2:
            seeable = False
        return seeable

    ########################################################################################
    # plot_view plot the field of view of the agent at time t
    #    inputs
    #        ax (pyplot.Axes): set of Axes to plot on
    #        t (int): time at which to plot field of view
    #    outputs:
    #        artists (list): list of type matplotlib.artist.Artist, which will have the artists for all things drawn on ax by
    #                             this method
    ########################################################################################

    def plot_view(self, ax, t):

        artists = []

        # plot the straight-out camera view
        pos = self.pos(t)
        cen = self.path.center
        slope = (pos[1] - cen[1]) / \
            (pos[0] - cen[0])
        b = pos[1] - slope * pos[0]
        if not self.path.clockwise:  # line goes toward center
            x = np.linspace(pos[0], cen[0], 50)
        else:
            x = np.linspace(pos[0], 2 * pos[0] - cen[0], 50)
        y = slope * x + b
        lines = ax.plot(x[:10], y[:10], 'k--')
        artists.append(lines)

        # plot the camera view angles
        a = self.camera_view_angle / 2
        x1 = (x - pos[0]) * np.cos(a) - (y - pos[1]) * np.sin(a) + pos[0]
        y1 = (x - pos[0]) * np.sin(a) + (y - pos[1]) * np.cos(a) + pos[1]
        lines = ax.plot(x1, y1, 'k')
        artists.append(lines)

        x2 = (x - pos[0]) * np.cos(-a) - (y - pos[1]) * np.sin(-a) + pos[0]
        y2 = (x - pos[0]) * np.sin(-a) + (y - pos[1]) * np.cos(-a) + pos[1]
        lines = ax.plot(x2, y2, 'k')
        artists.append(lines)

        # plot the position of agent at time t
        points = ax.scatter(pos[0], pos[1], c='k')
        artists.append(points)

        for tpos in self.target_pos:
            if self.can_see(t, tpos):
                mkr = '^'
            else:
                mkr = 'x'
            points = ax.scatter(tpos[0], tpos[1], marker=mkr, color='k')
            artists.append(points)
        return artists

##################################################################################
# A Scenario of the multivehicle target location problem
##################################################################################


class Scenario:

    ########################################################################################
    # __init__: Initialize an instance of class Scenario
    #    inputs
    #        num_agents (int): the number of agents in the scenario
    #        num_targets (int): the number of targets in the scenario
    #        k (int): the number of measurements to send to the satellite
    #        m (int): the number of measurements to share with future agents
    #        randomly_generate (bool): if True, randomly generate the agent starting points, paths and target starting points
    #        agent_start_positions (list): list of size num_agents of 2-tuples which represent the starting positions of each agent.
    #                                                      all numbers should be between max_coord and min_coord.
    #                                                      example: [(2, 30), (-20, 5), (-3.24, -11.11)]
    #        agent_end_positions (list): list of size num_agents of 2-tuples which represent the end positions of each agent.
    #                                                     see agent_start_positions for example.
    #        agent_dist (list): list of size num_agents of float or int. each path is the section of a circle, where the start and
    #                                                      end points are given in agent_start_positions and agent_end_positions, respectively.
    #                                                      the center of the circle for agent i is agent_dist[i] away from the midpoint between the start
    #                                                      and end of the path, along the bisecting line between them. negative runs one direction
    #                                                      and positive runs the other. example: [3, 300, -20.76]
    #        target_start_positions (list): list of size num_targets of 2-tuples which represent the locations of each target.
    #                                                       see agent_start_positions for example
    #        camera_view_angle (float): the field of view for every agent, centered out of the left side of the vehicle
    #        seed (int): random seed, used to re-create scenarios
    #    outputs: None
    ########################################################################################

    def __init__(self, num_agents, num_targets, k, m, randomly_generate=True, agent_start_positions=None,
                 agent_end_positions=None, agent_dist=None, target_start_positions=None, camera_view_angle=np.pi, seed=None):
        if seed:
            np.random.seed(seed)
        self.seed = seed
        self.num_agents = num_agents
        self.num_targets = num_targets
        self.k = k
        self.m = m
        self.Q0 = np.zeros((2 * num_targets, 2 * num_targets))
        self.sim_results = {}

        # generate paths and target locations
        if randomly_generate:
            agent_start_positions = np.random.randint(
                min_coord, max_coord, (num_agents, 2))
            agent_end_positions = np.random.randint(
                min_coord, max_coord, (num_agents, 2))
            agent_dist = np.random.rand(num_agents) * 2 * max_dist - max_dist

            target_start_positions = np.random.rand(
                num_targets, 2) * (max_coord - min_coord) + min_coord
            target_start_positions = np.array(
                [[t[0], t[1]] for t in target_start_positions])

        # create agent objects
        agent_colors = ['r', 'b', 'g', 'k', 'y', 'c']
        agent_colors += ['c' for i in range(num_agents - len(agent_colors))]
        agent_paths = [ScenarioPath(
            agent_start_positions[i], agent_end_positions[i], agent_dist[i]) for i in range(num_agents)]
        self.agents = [ScenarioAgent(path, i, target_start_positions, camera_view_angle, agent_colors[i])
                       for i, path in enumerate(agent_paths)]
        self.target_pos = target_start_positions

    ########################################################################################
    # _all_blind determines whether all agents are blind to all the targets
    #    inputs: None
    #    outputs
    #        (bool): False if there exists an agent which sees a target, True otherwise
    ########################################################################################

    def _all_blind(self):
        all_blind = True
        for agent in self.agents:
            for target in range(self.num_targets):
                tpos = self.target_pos[target]
                for t in range(num_pts):
                    if agent.can_see(t, tpos):
                        all_blind = False
                        break
                if not all_blind:
                    break
            if not all_blind:
                break
        return all_blind

    def generate_fims(self):
        # create Q0
        prior_var = 1e-6
        self.Q0 = np.eye(2 * self.num_targets) * (prior_var)

        # set other parameters
        fx = 50  # focal length
        ox = 0  # x-offset
        std_dev = 1  # std dev of prior (assume normal distribution)
        sig = np.array([[std_dev**2]])  # set variance
        A = np.array([[fx, ox], [0, 1]])  # camera model
        min_fim_log_det = np.inf
        upA = np.array([[1, 0]]) @ A
        downA = np.array([[0, 1]]) @ A

        ########################################################################################
        # gen_fim generate a Fisher Information Matrix and add it to the agent's fim list. this is a block diagonal matrix of the
        #               target fims
        #    inputs
        #        agent (ScenarioAgent): agent to get fim for
        #        t (int): time instance to get fim for
        #    outputs: None
        ########################################################################################

        def gen_fim(agent, t):
            # get the camera rotation matrix
            pos = agent.pos(t)
            theta = -agent.path.theta[t] - np.pi / 2
            if not agent.path.clockwise:
                theta += np.pi
            R = np.array([[np.cos(theta), -np.sin(theta)],
                          [np.sin(theta), np.cos(theta)]])  # rotation matrix

            ########################################################################################
            # target_fim generate a small fim
            #    inputs
            #        target (int): target number
            #    outputs
            #        (2x2 numpy array): fim for agent at time t for target
            ########################################################################################

            def target_fim(target):
                # test whether camera can see the target
                tpos = self.target_pos[target]
                if not agent.can_see(t, tpos):
                    return np.zeros((2, 2))
                # create the fim
                M = upA @ R
                m = downA @ R
                U = M @ (tpos - pos)
                d = m @ (tpos - pos)
                dIdtheta = M / d - U / (d**2) @ m
                return np.transpose(dIdtheta) @ sig @ dIdtheta

            fim = block_diag(*(target_fim(target)
                               for target in range(self.num_targets)))
            agent.add_fim(fim, t)

        [gen_fim(agent, t) for agent in self.agents for t in range(num_pts)]

    ########################################################################################
    # sim simulates the scenario
    #    inputs
    #        algs (list): a list of str, indicating which algorithms should be simulated.
    #                          available choices: ['random', 'greeedy', 'augmented_greedy']
    #        visualize (list): a list of bool of size len(algs). visualize[i] = True means that a set of plots will be shown to
    #                                 visualize how the agent is making decisions
    #    outputs
    #        self.sim_results (dict): a structure of the results of the simulation. an example looks like:
    #                                              {'random': {'score': 12.045, 'agents': {0:{'chosen': [0, 4], 'passed': [5, 8]}, 1:{'chosen':[0, 2], 'passed':[4, 1]}}},
    #                                               'greedy':  {'score': 15.556, 'agents': {0:{'chosen': [3, 5], 'passed': [1, 4]}, 1:{'chosen':[6, 0], 'passed':[1, 9]}}}}
    ########################################################################################

    def sim(self, algs=['random'], visualize=[False]):
        self.sim_results = {alg: {'score': 0, 'agents': {agent.agent_id: {'chosen': [], 'passed': []}
                                                         for agent in self.agents}} for alg in algs}
        ax = None

        # test whether some agent can see some target
        if self._all_blind():
            return self.sim_results

        self.generate_fims()

        ########################################################################################
        # sim_alg run the simulation for a particular algorithm and store results in self.sim_results
        #    inputs
        #        alg (str): the name of the algorithm. see __init__ for possible values
        #        i (int): algs[i] = alg
        #        ax (matlab.pyplot.Axes): Axes object for plotting visualization
        #   outputs: None
        ########################################################################################

        def sim_alg(alg, i, ax):
            if visualize[i]:
                plt.close()
                fig, axs = plt.subplots(1, 2, figsize=[10, 5])
                ax = axs[0]
                ax.set_xlim(left=-max_coord, right=max_coord)
                ax.set_ylim(bottom=-max_coord, top=max_coord)
                artists = self._build_base_axes(ax, include_targets=True)
                fig.waitforbuttonpress()
                for artist in artists:
                    if type(artist) == list:
                        for m in artist:
                            m.remove()
                    else:
                        artist.remove()
            fim_sent = np.zeros(
                (self.num_targets * 2, self.num_targets * 2))
            fims_passed = np.zeros(
                (0, 2 * self.num_targets, 2 * self.num_targets))
            pc_inds = set()  # indices of fims that have been passed and then chosen by another agent
            for agent in self.agents:
                fim_sent, fims_passed, c_inds, p_inds = agent.choose(
                    fim_sent, fims_passed, pc_inds, self.f, self.k, self.m, method=alg, ax=ax, oafun=self._plot_other_agent)
                pc_inds |= set([i if i >= num_pts else -1 for i in c_inds])
                self.sim_results[alg]['agents'][agent.agent_id]['chosen'] = c_inds
                self.sim_results[alg]['agents'][agent.agent_id]['passed'] = p_inds
            self.sim_results[alg]['score'] = self.f([fim_sent])

        # run the simulations. if we are visualizing one of the algorithms, we won't vectorize the functions
        if any(visualize):
            for i, alg in enumerate(algs):
                sim_alg(alg, i, ax)
        else:
            [sim_alg(alg, i, ax) for i, alg in enumerate(algs)]

        return self.sim_results

    ########################################################################################
    # f return the function value
    # inputs
    #    fims (list): a list of fim matrices
    # outputs
    #    (float): the value of f for the fims
    ########################################################################################

    def f(self, fims):
        F = sum(fims) + self.Q0
        res = np.log(np.linalg.det(F)) - np.log(np.linalg.det(self.Q0))
        return res

    # its assumed that ind >= num_pts
    ########################################################################################
    # _plot_other_agent plot something about an agent
    #    inputs
    #        ind (int): the index of the fim, assuming that ind >= num_pts. for instance, if num_pts=10, and ind=10, then the
    #                       plotting will refer to the timestep of the first measurement that the first agent passed
    #        ax (matplotlib.pyplot.Axes): Axes object to make the plot on
    #        alg (str): the name of the algorithm - see __init__ for possible options
    #        plot_type (str): the type of plot to make. possible choices: 'view', the plot that shows the agent's view; '
    ########################################################################################

    def _plot_other_agent(self, ind, ax, alg, plot_type='view', marker_color='k'):
        from_agent_id = int(np.floor((ind - num_pts) / self.m))
        p = (ind - num_pts) % self.m
        from_agent = self.agents[from_agent_id]
        t = self.sim_results[alg]['agents'][from_agent_id]['passed'][p]
        if plot_type == 'view':
            artists = from_agent.plot_view(ax, t)
        else:
            pos = from_agent.pos(t)
            artist = ax.scatter(
                pos[0], pos[1], c=marker_color, marker='X')
            artists = [artist]
        return artists

    ########################################################################################
    # _get_chosen_pos gets the positions of all the chosen measurements for an agent and algorithm. note that if the
    #                               measurement chosen was actually taken by another agent, the position of that agent is given
    #    inputs
    #        agent_id (int): self.agents[agent_id] is the agent whose measurements we're getting
    #        alg (str): name of algorithm to get the measurements for
    #    outputs
    #        pos (list): a list of 2-tuples, indicating the positions at which the measurements were taken that were chosen
    #                         by the agent using the algorithm. example: [(2, -3), (4.9, 100.5), (-40.78, 0)]
    ########################################################################################

    def _get_chosen_pos(self, agent_id, alg):
        c_inds = self.sim_results[alg]['agents'][agent_id]['chosen']
        pos = []
        for ind in c_inds:
            if ind < num_pts:
                pos.append(self.agents[agent_id].pos(ind))
            else:
                from_agent_id = int(np.floor((ind - num_pts) / self.m))
                p = (ind - num_pts) % self.m
                p_ind = self.sim_results[alg]['agents'][from_agent_id]['passed'][p]
                pos.append(self.agents[from_agent_id].pos(p_ind))
        return pos

    ########################################################################################
    # _build_base_axes add the agent paths and target locations to the axes given
    #    inputs
    #        ax (matplotlib.pyplot.Axes): Axes object to add stuff to
    #        include_targets (bool): if True, target locations will be added to the Axes
    #    outputs
    #        artists (list): a list of artists for the target locations points
    ########################################################################################

    def _build_base_axes(self, ax, include_targets=True):
        artists = []
        if include_targets:
            points = ax.scatter([t[0] for t in self.target_pos], [t[1]
                                                                  for t in self.target_pos])
            artists.append(points)

        agent_colors = ['r', 'b', 'g', 'k', 'y', 'c']
        for j, agent in enumerate(self.agents):
            agent.path.plot(agent_colors[j], ax=ax)
        return artists

    ########################################################################################
    # plot plot the results of the simulation.
    #    inputs
    #        subplot_rows (int): number of rows on the subplot. note that len(self.sim_results) should not be greater than
    #                                        subplot rows x subplot_columns
    #        subplot_columns (int): number of columns on the subplot
    #    outputs: None
    ########################################################################################

    def plot(self, subplot_rows=1, subplot_columns=2):
        fig, axes = plt.subplots(
            subplot_rows, subplot_columns, figsize=[10, 5])
        axes_flat = axes.flatten()
        meas_mkr = 'X'

        i = 0
        for alg, dct in self.sim_results.items():
            ax = axes_flat[i]
            ax.set_title(alg)
            ax.set_xlim(left=-max_coord, right=max_coord)
            ax.set_ylim(bottom=-max_coord, top=max_coord)
            self._build_base_axes(ax)

            for agent_id in range(len(self.agents)):
                pos = self._get_chosen_pos(agent_id, alg)
                ax.scatter([x[0] for x in pos], [x[1] for x in pos],
                           marker=meas_mkr, c=self.agents[agent_id].color, s=50)
            i += 1

        fig.suptitle('seed: {}'.format(self.seed))
        plt.show()

    ########################################################################################
    # plot_targets_viewed plot the view of an agent at time t. all targets not seen by the agent will be marked as an 'x',  all
    #                                   targets seen will be marked by a triangle
    #    inputs
    #        agent_num (int): agent id for agent whose view will be plotted
    #        t (int): time at which the agent's view should be plotted
    #    outputs: None
    ########################################################################################

    def plot_targets_viewed(self, agent_num, t):
        fig = plt.figure()
        ax = fig.add_subplot(111)

        self._build_base_axes(ax, include_targets=False)
        agent = self.agents[agent_num]
        agent.plot_view(ax, t)

        # plt.savefig('targets.png')
        ax.set_title('Agent {} at time {}'.format(agent_num, t))
        plt.show()

    def nash_equilibrium(self, edges):

