import istarmap
from Scenario import Scenario
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
from scipy.io import loadmat
from multiprocessing import Pool
import itertools as it

k = 2
m = 2
agents = [3]
targets = [3]
num_trials = 1


def run_trial(trial, nt, na):
    np.random.seed()
    seed = np.random.randint(2**32)
    seed = 1264611317
    scen = Scenario(na, nt, k, m, camera_view_angle=np.pi / 2, seed=seed)
    results = scen.sim(
        algs=['greedy_opt', 'augmented_greedy_opt'], visualize=[False, False])
    g = results['greedy_opt']['score']
    ag = results['augmented_greedy_opt']['score']
    if g == 0:
        comp = 0
    else:
        comp = ag / g

    if num_trials == 1:
        print('\n\ngreedy: {}, greedy_opt: {}'.format(
            results['greedy_opt']['score'], results['augmented_greedy_opt']['score']))
        scen.plot()
    return seed, g, ag, comp, na, nt


with Pool() as p:
    data = [res for res in tqdm(p.istarmap(run_trial, it.product(range(
        num_trials), targets, agents)), total=num_trials * len(agents) * len(targets))]

if num_trials > 1:
    df = pd.DataFrame(data, columns=[
                      'seed', 'greedy_score', 'augmented_greedy_score', 'improvement', 'num_agents', 'num_targets'])
    df.to_csv('trials.csv')
    vec = df[df.improvement > 0]['improvement']
    print('number of trials removed: {}'.format(num_trials - len(vec)))

    plt.hist(vec, bins=100)
    #d = loadmat('tmp/bin_edges.mat')
    #plt.hist(vec, bins=d['be'][0])

    s = 'Improvement on Augmented Greedy over Nominal Greedy'
    s += '\nAvg: {}'.format(round(vec.mean(), 3))
    s += ', Min: {}'.format(round(min(vec), 3))
    s += ', Max: {}'.format(round(max(vec), 3))
    s += ', % Improved: {}%'.format(
        round(len(vec[vec > 1]) / len(vec), 3) * 100)
    plt.title(s)
    s += ', % Not Decreased: {}%'.format(
        round(len(vec[vec >= 1]) / len(vec), 3) * 100)
    plt.show()
